/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package argstest;

/**
 *
 * @author gdunkley
 */
public class ArgsTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // TODO code application logic here
        
        System.out.println("Program Arguments: ");
        for (String arg : args) {
            System.out.println("\t" + arg);
        }
        
        System.out.println();
        System.out.println(args[0]);
        System.out.println(args[5]);
        System.out.println(args[2]);
        System.out.println(args[4]);
        System.out.println(args[1]);
        
    }
    
}
