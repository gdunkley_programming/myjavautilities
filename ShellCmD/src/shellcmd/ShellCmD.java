/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shellcmd;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


/**
 *
 * @author gdunkley
 */
public class ShellCmD {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException{
        
//        List<String> params = Arrays.asList("ipconfig", "/all");
//        ProcessBuilder pb = new ProcessBuilder(params);
//        pb.start();
//        BufferedReader output = pb.redirectOutput();
        
        callCmd("which bash");

        callCmd("/HORCM/usr/bin/raidcom get ldev -ldev_list mapped -key tier " 
                + "-s 53280 -cnt 10 " 
                + "| awk '{$1=$1};1' ");
    
    }
            
        /** Returns null if it failed for some reason.
         * @param cmdline
         * @param directory
         * @return 
        */
        public static ArrayList<String> command(final String cmdline, final String directory) {
            try {
                Process process = new ProcessBuilder(new String[] {"bash", "-c", cmdline})
                        .redirectErrorStream(true)
                        .directory(new File(directory))
                        .start();

                ArrayList<String> output = new ArrayList<String>();
                BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String line = null;
                while ( (line = br.readLine()) != null ){
                    output.add(line);
                }
                //There should really be a timeout here.
                if (0 != process.waitFor()){
                    return null;
                }
                return output;

            } catch (Exception e) {
            //Warning: doing this is no good in high quality applications.
            //Instead, present appropriate error messages to the user.
            //But it's perfectly fine for prototyping.

            return null;
            }
    }
        
        static void callCmd(String cmdline) {
            ArrayList<String> output = command(cmdline, ".");
            if (null == output){   
                System.out.println("\n\n\t\tCOMMAND FAILED: " + cmdline);
            }    
            else{
                for (String line : output){
                    System.out.println(line);
                }
            }
        }
}

//arrayLine[i] = line;
//                i++;