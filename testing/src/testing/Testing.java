/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

/**
 *
 * @author gdunkley
 */
public class Testing {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        String[] uniqueVals = new String[]{"1", "2", "3"};
        
        int count = 0;
        int countMax = 7;
        for (String strVal: uniqueVals){
            System.out.println("1st for loop statement starts");
            if (strVal !=null){
                System.out.println("1st if statement starts");
                System.out.println(strVal + " - ");
                System.out.println("append 'strVal'");
//                strBld2.append("'").append(strVal).append("'");
                count++;
                System.out.println("count is now at :" + count);
                if (count != countMax){
                    System.out.println("2nd if statement starts");
                    System.out.println("count is now at :" + count);
                    System.out.println("append ,");
//                    strBld2.append(", ");
                }
            }
        }
        System.out.println("\n\n");
        for (int i=count; i <= countMax; i++){
            System.out.println("2nd for statement starts");
            System.out.println("count is now at :" + count);
            System.out.println("i is now at : " + i);
            
            if (count < countMax){
                System.out.println("count is now at :" + count);
                System.out.println("append null, ");
                count++;
//                strBld2.append("null, ");
            }
            else if (count == countMax){
                System.out.println("else if statement started");
                System.out.println("count is now at :" + count);
                System.out.println("append null) ");
            }

        }
        System.out.println("count is now at :" + count);
        System.out.println("append ) ");
//        strBld2.append(")");
                
                
                
    }
    
}
