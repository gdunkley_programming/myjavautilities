 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools |  Templates
 * and open the template in the editor.
 */
package javazip;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JavaZip {

    public static void main(String[] args) throws IOException {
        
        /**
         * Zip Example 1 - recursive
         */
        String suffix = new SimpleDateFormat("yyyyMMdd_hhmmss'.zip'").format(new Date());
        String zipFile = "C:/02-WorkStor/VSPTestArchive_" + suffix;
        String srcDir = "C:/02-WorkStor/VSPTest";
        
        try {
            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos);
            File srcFile = new File(srcDir);
            zipTypes obj1 = new zipTypes();
            obj1.addDirToArchive(zos, srcFile);
            // close the ZipOutputStream
            zos.close();
        }
        catch (IOException ioe) {
            System.out.println("Error creating zip file: " + ioe);
        }
        
        /**
         * Zip Example 2 - recursive keeping directry structure
         */
        
        
        File mydir = new File("C:/02-WorkStor/VSPTest");
        File myfile = new File("C:/02-WorkStor/myfile.zip");
        System.out.println(mydir.toURI().relativize(myfile.toURI()).getPath());
        
        zipTypes obj2 = new zipTypes();
        obj2.zipRecursive(mydir, myfile);
        
    }
}


