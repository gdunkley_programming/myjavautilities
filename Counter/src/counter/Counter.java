/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package counter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.lang.String;

/**
 *
 * @author gdunkley
 */
public class Counter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       
       
       
       for (int j=0; j<1000; j++)
       {
           //           System.out.println("j= " + j + "count= " + count);
           
           int count = j % 10;
           int count1 = j % 100;
           if (count == 0)
           {
               System.out.print("*");
           }
           if (count1 == 0){
               System.out.println();
               System.out.println("Line : " + j + " hit!");
           }
       }
       
       System.out.println();
       System.out.println();
       System.out.println();
       
       
       List<String> list = Arrays.asList("A", "B", "C", "D", "E");
       
       for (int i = 2; i < list.size(); i++) 
       {
           for (int j = i+1; j < list.size(); j++) 
           {
               System.out.println("ij=" + i + j + "   list=" + list.get(i) + list.get(j));
               // compare list.get(i) and list.get(j)
           }
       }
       
       
       List<String> gasList = Arrays.asList("A", "B", "C", "D", "E", "A", "B", "C", "D", "E", "A", "B", "C", "D", "E");
       Set<String> uniqueGas = new HashSet<>(gasList);
       System.out.println("Unique gas count: " + uniqueGas.size());
       for (String val: uniqueGas)
       {
           System.out.println(val);
       } 
    }
    
}
