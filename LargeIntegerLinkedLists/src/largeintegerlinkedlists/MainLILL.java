/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package largeintegerlinkedlists;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author gdunkley
 */
public class MainLILL {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String txtFile = args[0];
        
        // decleare object and open file/read file.
        String strNum1 = null;
        String strNum2 = null;
        
        
        try 
        {
            Scanner input = new Scanner(new File(txtFile));
            strNum1 = input.next();
            strNum2 = input.next();
            System.out.println("The file :" + txtFile + " has been read successfully.");
            System.out.println();
                        
        } 
        catch (FileNotFoundException e) 
        {
            System.out.println("Error - File dosn't exist or not specified in Arguments \n" 
                    + "Please check the filename and path. Error is: \n\n"
                    + e);
            System.out.println();
        }
        
        IntLinkedList list1 = new IntLinkedList();
        
        list1 = list1.sill(strNum1);
        list1.Display();
        System.out.println(list1.getIsNegative());
        list1.sillReverseDisplay();
        
        
        
        
        
        
//		IntListStack Main method testing examples:
//
//		IntListStack s = new IntListStack();
//		
//		for(int i = 0; i < 5; i++)
//		{
//			System.out.println("Pushed " + i);
//			s.push(i);
//		}
//		System.out.println();
//
//		while(!s.empty())
//		{
//			System.out.println("3 is at postion " + s.search(3));
//			System.out.println("Popped " + s.pop());
//		}
//		
//		// cause EmptyStackException
//		try
//		{
//			System.out.println("Popped " + s.pop());
//			throw new Error("This never happens");
//		}
//		catch (Exception e)
//		{
//			System.out.println("caught " + e);
//		}

        
        
//        EndofMain
    }
    
}
