package largeintegerlinkedlists;

import java.util.EmptyStackException;

/**
 * Implement a stack of integers using a linked list 
 *
 */
public class IntListStack {
	
	private class IntNode
	{
		int data;
		IntNode link;
		
		IntNode(int data, IntNode link)
		{
			this.data = data;
			this.link = link;
		}
	}
	
	private IntNode tos;  // top of stack
	
	/**
	 * Create and empty stack
	 */
	IntListStack()
	{
		tos = null;
	}
	
	/**
	 * Pushes an item onto the top of this stack.
	 * @param data
	 */
	public void push(int data)
	{
		tos = new IntNode(data, tos);
	}
	
	/**
	 *  Tests if this stack is empty.
	 * @return true if empty
	 */
	public boolean empty()
	{
		return tos == null;
	}
	
	/**
	 * Removes the data at the top of this stack and returns it.
	 * @return data from top of stack
	 * @throws EmptyStackException
	 */
	public int pop() throws EmptyStackException
	{
		if(empty())
			throw new EmptyStackException();
		else
		{
			int result = tos.data;
			tos = tos.link;
			return result;
		}
	}
	
	/**
	 *  Looks at the object at the top of this stack without removing it from the stack.
	 * @return data from top of stack
	 * @throws EmptyStackException
	 */
	public int peek() throws EmptyStackException
	{
		if(empty())
			throw new EmptyStackException();
		else
			return tos.data;
	}
	
	
	/**
	 * Returns the 1-based position where data is found on this stack.
	 * @param target  data to search for
	 * @return -1 if data not found
	 */
	public int search(int target)
	{
	      IntNode cursor;
	      int position = 1;
	      
	      for (cursor = tos; cursor != null; cursor = cursor.link)
	      {
	    	  if (target == cursor.data)
	    		  return position;
	    	  position ++;
	      }
	        
	      return -1;

	}     
}