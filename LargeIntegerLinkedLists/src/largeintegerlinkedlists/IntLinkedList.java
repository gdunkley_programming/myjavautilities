/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package largeintegerlinkedlists;

// File: IntLinkedList.java from the package edu.colorado.linked
// Complete documentation is available from the IntLinkedList link in:
//   http://www.cs.colorado.edu/~main/docs

//package edu.colorado.collections;
//import IntNode;


/******************************************************************************
* An IntLinkedList is a collection of int numbers.
*
* @note
*   (1) Beyond Int.MAX_VALUE elements, countOccurrences,
*   size, and grab are wrong.
*   <p>
*   (2) Because of the slow linear algorithms of this class, large bags will have
*   poor performance.
*
* @see
*   <A HREF="../../../../edu/colorado/collections/IntLinkedList.java">
*   Java Source Code for this class
*   (www.cs.colorado.edu/~main/edu/colorado/collections/IntLinkedList.java)
*   </A>
*
* @author Michael Main 
*   <A HREF="mailto:main@colorado.edu"> (main@colorado.edu) </A>
*
* @version
*   March 6, 2002
*
* @see IntArrayBag
* @see LinkedBag
******************************************************************************/
public class IntLinkedList implements Cloneable
{
    // Invariant of the IntLinkedList class:
    //   1. The elements in the bag are stored on a linked list.
    //   2. The head reference of the list is in the instance variable 
    //      head.
    //   3. The total number of elements in the list is in the instance 
    //      variable manyNodes.
    private IntNode head;
    private int manyNodes;   
    boolean isNegative;

    /**
    * Initialize an empty bag.
    * @postcondition
    *   This bag is empty.
    **/
    public IntLinkedList( )
    {
        head = null;
        manyNodes = 0;
        
    }


    /**
    * Add a new element to this bag.
    * @param element
    *   the new element that is being added
    * @postcondition
    *   A new copy of the element has been added to this bag.
    * @exception OutOfMemoryError
    *   Indicates insufficient memory a new IntNode.
    **/
    public void add(int element)
    {      
        head = new IntNode(element, head);
        manyNodes++;
    }


    /**
    * Add the contents of another bag to this bag.
    * @param addend
    *   a bag whose contents will be added to this bag
    * @precondition
    *   The parameter, addend, is not null.
    * @postcondition
    *   The elements from addend have been added to this bag.
    * @exception NullPointerException
    *   Indicates that addend is null.
    * @exception OutOfMemoryError
    *   Indicates insufficient memory to increase the size of the bag.
    **/
    public void addAll(IntLinkedList addend)
    {
        IntNode[ ] copyInfo;

        // The precondition indicates that addend is not null. If it is null,
        // then a NullPointerException is thrown here.
        if (addend.manyNodes > 0)
        {
            copyInfo = IntNode.listCopyWithTail(addend.head);
            copyInfo[1].setLink(head); // Link the tail of copy to my own head... 
            head = copyInfo[0];        // and set my own head to the head of the copy.
            manyNodes += addend.manyNodes;
        }
    }


    /**
    * Add new elements to this bag. If the new elements would take this
    * bag beyond its current capacity, then the capacity is increased
    * before adding the new elements.
    * @param elements
    *   (a variable-arity argument)
    *   one or more new elements that are being inserted
    * @postcondition
    *   A new copy of the element has been added to this bag.
    * @exception OutOfMemoryError
    *   Indicates insufficient memory to increase the size of the bag.
    **/
    public void addMany(int... elements)
    {
        // Activate the ordinary add method for each integer in the
        // elements array.
        for (int i : elements)
            add(i); 	       
    }


    /**
    * Generate a copy of this bag.
    * @param - none
    * @return
    *   The return value is a copy of this bag. Subsequent changes to the
    *   copy will not affect the original, nor vice versa. Note that the return
    *   value must be type cast to an IntLinkedList before it can be used.
    * @exception OutOfMemoryError
    *   Indicates insufficient memory for creating the clone.
    **/ 
    public Object clone( )
    {  // Clone a nIntLinkedList object.
        IntLinkedList answer;

        try
        {
            answer = (IntLinkedList) super.clone( );
        }
        catch (CloneNotSupportedException e)
        {
            // This exception should not occur. But if it does, it would probably
            // indicate a programming error that made super.clone unavailable.
            // The most common error would be forgetting the "Implements Cloneable"
            // clause at the start of this class.
            throw new RuntimeException
            ("This class does not implement Cloneable");
        }
        answer.head = IntNode.listCopy(head);

        return answer;
    }


    /**
    * Accessor method to count the number of occurrences of a particular element
    * in this bag.
    * @param target
    *   the element that needs to be counted
    * @return
    *   the number of times that target occurs in this bag
    **/
    public int countOccurrences(int target)
    {
        int answer;
        IntNode cursor;

        answer = 0;
        cursor = IntNode.listSearch(head, target);
        while (cursor != null)
        {
            // Each time that cursor is not null, we have another occurrence of
            // target, so we add one to answer and then move cursor to the next
            // occurrence of the target.
            answer++;
            cursor = cursor.getLink( );
            cursor = IntNode.listSearch(cursor, target);
        }
        return answer;
    }


    /**
    * Accessor method to retrieve a random element from this bag.
    * @precondition
    *   This bag is not empty.
    * @return
    *   a randomly selected element from this bag
    * @exception IllegalStateException
    *   Indicates that the bag is empty.
    **/
    public int grab( )
    {
        int i;
        IntNode cursor;
        
        if (manyNodes == 0)
            throw new IllegalStateException("Bag size is zero");

        i =  (int)(Math.random( ) * manyNodes) + 1;
        cursor = IntNode.listPosition(head, i);
        return cursor.getData( );
    }


    /**
    * Remove one copy of a specified element from this bag.
    * @param target
    *   the element to remove from the bag
     * @return 
    * @postcondition
    *   If target was found in the bag, then one copy of
    *   target has been removed and the method returns true. 
    *   Otherwise the bag remains unchanged and the method returns false. 
    **/
    public boolean remove(int target)
    {
        IntNode targetNode; // The node that contains the target

        targetNode = IntNode.listSearch(head, target);
        if (targetNode == null)
         // The target was not found, so nothing is removed.
            return false;
        else
        {
            // The target was found at targetNode. So copy the head data to targetNode
            // and then remove the extra copy of the head data.
            targetNode.setData(head.getData( ));
            head = head.getLink( );
            manyNodes--;
            return true;
        }
    }
    
    public int get(int position)
    {
//        int pos = position;
        IntNode cursor;
        if (manyNodes == 0)
        {
            throw new IllegalStateException("List size is zero");
        }
        cursor = IntNode.listPosition(head, position);
        return (int) cursor.getData();
        
    }

    /**
    * Determine the number of elements in this bag.
    * @return
    *   the number of elements in this bag
    **/                           
    public int size( )
    {
      return manyNodes;
    }


    /**
    * Create a new bag that contains all the elements from two other bags.
    * @param b1
    *   the first of two bags
    * @param b2
    *   the second of two bags
    * @precondition
    *   Neither b1 nor b2 is null.
    * @return
    *   the union of b1 and b2
    * @exception IllegalArgumentException
    *   Indicates that one of the arguments is null.
    * @exception OutOfMemoryError
    *   Indicates insufficient memory for the new bag.
    **/   
    public static IntLinkedList union(IntLinkedList b1, IntLinkedList b2)
    {       
      // The precondition requires that neither b1 nor b2 is null.
      // If one of them is null, then addAll will throw a NullPointerException.  
      IntLinkedList answer = new IntLinkedList( );

      answer.addAll(b1);
      answer.addAll(b2);     
      return answer;
    }
    
   
   
    public void Display()
    {       
        IntNode cursor = head;     
        while (cursor != null)
        {
            System.out.print(cursor.getData());
            cursor = cursor.getLink();
        }
        System.out.println();
    }
    
    
    
    public IntLinkedList sill(String newNumber)
    {
        IntLinkedList list = new IntLinkedList();
        StringBuilder sBuilder = new StringBuilder();
        for (int i=0; i<newNumber.length(); i++)
        {
            sBuilder.append(newNumber.charAt(i));
        }
        System.out.println();

        if (newNumber.startsWith("-"))
        {
            sBuilder.deleteCharAt(0);
            list.setIsNegative(true);
            for (int i=0; i<sBuilder.length(); i++)
            {
                list.add(Integer.parseInt(Character.toString(sBuilder.charAt(i))));                        
            }
        }
        else
        {
            for (int i=0; i<sBuilder.length(); i++)
            {       
                list.add(Integer.parseInt(Character.toString(sBuilder.charAt(i))));
            }
        }
        return list;
    }
    
    
    public void setIsNegative(boolean state)
    {
        this.isNegative = state;
    }
    
    public boolean getIsNegative()
    {
        return isNegative;        
    }
    
    
    
    public IntLinkedList mill(String newNumber)
    {
        IntLinkedList list = new IntLinkedList();
        IntListStack stk1 = new IntListStack();
        IntListStack stk2 = new IntListStack();
        StringBuilder sBuilder = new StringBuilder(3);
        
        int count=0;
        for (int i=newNumber.length()-1; i>=0; i--)
        {
            if (count==3)
            {
                do 
                {
                    sBuilder.append(stk1.pop());
                    if (stk1.empty())
                    {
                        stk2.push(Integer.parseInt(sBuilder.toString()));
                    }
                } while (!stk1.empty());
                count=0;
                sBuilder = new StringBuilder(3);
            }
            stk1.push(newNumber.charAt(i));
            count++;
        }
        do 
        {
            sBuilder.append(stk1.pop());
            if (stk1.empty())
            {
                stk2.push(Integer.parseInt(sBuilder.toString()));
            }
        } while (!stk1.empty());
        do
        {
            list.add(stk2.pop());
            
        } while (!stk2.empty());
        return list;
    }
    
    
    public void sillReverseDisplay()
    {
        IntNode cursor = head;                
        System.out.println("The number of nodes in the list is: " + IntNode.listLength(cursor));
        int count =0;
        IntListStack stackLL = new IntListStack();
        
        StringBuilder sBuilder = new StringBuilder();
        
                
//        String comma = ",";
//        System.out.println(Integer.parseInt(Character.toString(comma.charAt(0))));
//        list.add(Integer.parseInt(Character.toString(sBuilder.charAt(i))));   
        while (cursor != null)
        {
            
            if (count == 3)
            {
                sBuilder.append(",");
//                stackLL.push(comma.charAt(0));
                count = 0;
            }
            sBuilder.append(cursor.getData());
//            stackLL.push(cursor.getData());
            cursor = cursor.getLink();
            count++;
        }
        sBuilder.reverse();
        System.out.println(sBuilder.toString());
        
//        while (!stackLL.empty()){
//            System.out.print(stackLL.pop());
//        }
        System.out.println();
    
    }
    
    
}
           
